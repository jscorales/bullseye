//
//  AboutViewController.h
//  BullsEye
//
//  Created by Junel Corales on 11/19/13.
//  Copyright (c) 2013 Junel Corales. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutViewController : UIViewController
@property (nonatomic, weak) IBOutlet UIWebView *webView;
-(IBAction)close;
@end
