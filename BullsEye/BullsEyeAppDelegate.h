//
//  BullsEyeAppDelegate.h
//  BullsEye
//
//  Created by Junel Corales on 10/29/13.
//  Copyright (c) 2013 Junel Corales. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BullsEyeAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
